package com.tsh.tutorials;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class YamlHandler 
{
	public static File createFile(String filename)
	{
		File f = new File(TutorialBukkit.getInstance().getDataFolder().getAbsolutePath() + File.separator + filename);
		if(!f.exists())
		{
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return f;
	}
	
	public static FileConfiguration createYamlFile(File f)
	{
		FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
		return fc;
	}
	
	public static void saveYamlFile(FileConfiguration c,File f)
	{
		try {
			c.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
