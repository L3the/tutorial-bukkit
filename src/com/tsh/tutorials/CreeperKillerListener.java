package com.tsh.tutorials;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class CreeperKillerListener implements Listener
{
  @EventHandler 
  void onCreeperKill(EntityDeathEvent e)
  {
	  if(e.getEntity().getType() == EntityType.CREEPER && e.getEntity().getKiller() instanceof Player)
	  {
		  Player p = e.getEntity().getKiller().getPlayer();
		  TutorialBukkit.getPointManager().addPoints(p, 1);
		  ScoreboardBeispiel.setExampleScoreboard(p);
		  ScoreboardBeispiel.setExampleScoreboardTab(p);
	  }
  }
}
