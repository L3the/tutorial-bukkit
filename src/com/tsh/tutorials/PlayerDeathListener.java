package com.tsh.tutorials;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener
{
 @EventHandler
 void onDeath(PlayerDeathEvent e)
 {
	 BossHealth.removeText(e.getEntity());
 }
}
