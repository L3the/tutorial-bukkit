package com.tsh.tutorials;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BossHealthShowEvent extends Event
{
	private static final HandlerList handlers = new HandlerList();
	private String bossname;
	
	public BossHealthShowEvent(String name)
	{
		bossname = name;
	}
	
	public String getText()
	{
		return bossname;
	}
	
	@Override
	public HandlerList getHandlers() {
		// TODO Auto-generated method stub
		return handlers;
	}

}
