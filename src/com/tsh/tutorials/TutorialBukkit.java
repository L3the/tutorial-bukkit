 package com.tsh.tutorials;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import com.tsh.tutorials.commands.CommandHandler;


public class TutorialBukkit extends JavaPlugin
{
  private static TutorialBukkit plugin;	
  private static FileConfiguration c;
  private static File f;
  private static PointManager p;
  public static HashMap<String,Scoreboard> scoreboardlist = new HashMap<String,Scoreboard>();
	
  public void onEnable()
  {
	  plugin = this;
	  System.out.println("Hallo, Welt! Wie geht es dir!");
	   f = YamlHandler.createFile("spawns.yml");
      c = YamlHandler.createYamlFile(f);
	
	  Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new PlayerInteractListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new BlockPlaceListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new BlockBreakListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new SeverListPingListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new CommandHandler(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new CreeperKillerListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new PlayerDeathListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new PlayerRespawnListener(), this);
	  Bukkit.getServer().getPluginManager().registerEvents(new BossHealthListener(), this);
	  
	  p = new PointManager();
	
	
	  
  }
  
  public void onDisable()
  {
	  System.out.println("Hallo, Welt! Es war sch�n!");
  }
  
  public static TutorialBukkit getInstance()
  {
	  return plugin;
  }
  
  public static FileConfiguration getSpawnFile()
  {
	  return c;
  }
  
  public static File getRawSpawnFile()
  {
	  return f;
  }
  
  public static PointManager getPointManager()
  {
	  return p;
  }
  
  
}
