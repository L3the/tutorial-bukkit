package com.tsh.tutorials;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardBeispiel 
{
  public static void setExampleScoreboard(Player p)
  {
	  if(!TutorialBukkit.scoreboardlist.containsKey(p.getName()))
	  {
	  Scoreboard playersboard = Bukkit.getScoreboardManager().getNewScoreboard();
	  Objective objective = playersboard.registerNewObjective("TEST!", "dummy");
	  objective.setDisplaySlot(DisplaySlot.SIDEBAR);
	  objective.setDisplayName("TEST!");
	  Score s1 = objective.getScore(Bukkit.getOfflinePlayer("Creeperkills: "));
	  s1.setScore(TutorialBukkit.getPointManager().getPoints(p));
	  p.setScoreboard(playersboard);
	  TutorialBukkit.scoreboardlist.put(p.getName(), playersboard);
	  }
	  else
	  {
		  Scoreboard playersboard = TutorialBukkit.scoreboardlist.get(p.getName());
		  Objective objective = playersboard.getObjective("TEST!");
		  objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		  objective.setDisplayName("TEST!");
		  Score s1 = objective.getScore(Bukkit.getOfflinePlayer("Creeperkills: "));
		  s1.setScore(TutorialBukkit.getPointManager().getPoints(p));
		  p.setScoreboard(playersboard);
	  }
     
  }
  
  public static void setExampleScoreboardTab(Player p)
  {
	  if(!TutorialBukkit.scoreboardlist.containsKey(p.getName()))
	  {
	  Scoreboard playersboard = Bukkit.getScoreboardManager().getNewScoreboard();
	  Objective objective = playersboard.registerNewObjective("TEST!", "dummy");
	  objective.setDisplaySlot(DisplaySlot.PLAYER_LIST);
	  objective.setDisplayName("TEST!");
	  Score s1 = objective.getScore(p);
	  s1.setScore(TutorialBukkit.getPointManager().getPoints(p));
	  
	  p.setScoreboard(playersboard);
	  
	  TutorialBukkit.scoreboardlist.put(p.getName(), playersboard);
	  }
	  else
	  {
		  Scoreboard playersboard = TutorialBukkit.scoreboardlist.get(p.getName());
		  Objective objective = playersboard.getObjective("TEST!");
		  objective.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		  objective.setDisplayName("TEST!");
		  Score s1 = objective.getScore(p);
		  s1.setScore(TutorialBukkit.getPointManager().getPoints(p));
		  p.setScoreboard(playersboard);
	  }
     
  }
  

}
