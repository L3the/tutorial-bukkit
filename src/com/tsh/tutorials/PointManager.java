package com.tsh.tutorials;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class PointManager 
{
 private static File f;
 private static FileConfiguration c;
 public PointManager()
 {
	f = YamlHandler.createFile("points.yml");
	c = YamlHandler.createYamlFile(f);
 }
 
 public void addPoints(Player p,int points)
 {
	 int currentpoints = getPoints(p);
	 
	 if(currentpoints == -1)
	 {
		 currentpoints = points;
	 }
	 else
		 currentpoints = getPoints(p) + points;
	 
	 c.set(p.getName(), currentpoints);
	 
	 YamlHandler.saveYamlFile(c, f);
 }
 
 public int getPoints(Player p)
 {
	 try
	 {
	 int points = c.getInt(p.getName());
	 
	 return points;
	 }
	 catch(Exception e)
	 {
		 return -1;
	 }
 }
 

}
