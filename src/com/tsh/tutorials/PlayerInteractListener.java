package com.tsh.tutorials;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener
{
   @EventHandler
   void onPlayerInteract(PlayerInteractEvent e)
   {
	   Action a = e.getAction();
	   
	   if(a == Action.RIGHT_CLICK_AIR)
	   {
		   e.getPlayer().sendMessage("Es wurde mit Rechts in die Luft geklickt.");
	   }
	   
	   if(a == Action.RIGHT_CLICK_BLOCK)
	   {
		   e.getPlayer().sendMessage("Es wurde mit Rechts auf einen Block geklickt.");
	   }
   }
}
