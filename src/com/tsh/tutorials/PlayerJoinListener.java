package com.tsh.tutorials;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener
{
   @EventHandler
   void onPlayerJoin(PlayerJoinEvent e)
   {
	for(Player p: Bukkit.getOnlinePlayers())
	{
		p.sendMessage("Hi, Spieler! Der Test hat geklappt :3");
	}
	
	FileConfiguration c = TutorialBukkit.getSpawnFile();
	int x = c.getInt("spawns.x");
	int y = c.getInt("spawns.y");
	int z = c.getInt("spawns.z");
	Location l = new Location(e.getPlayer().getWorld(),x,y,z);
	e.getPlayer().teleport(l);
	
	 e.setJoinMessage("�b" + e.getPlayer().getName() + " ist dem Spiel beigetreten!");
	 ScoreboardBeispiel.setExampleScoreboardTab(e.getPlayer());
	 BossHealth.displayText(e.getPlayer(), "Hier k�nnte Werbung stehen!", 1);
	 e.getPlayer().getInventory().addItem(HolySword.getSword());
   }
   
  
}
