package com.tsh.tutorials;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class HolySword 
{
 public static ItemStack getSword()
 {
	 ItemStack is = new ItemStack(Material.DIAMOND_SWORD);
	 ItemMeta im = is.getItemMeta();
	 im.setDisplayName("Heiliges Schwert der Heiligkeit");
	 List<String>  strings = new ArrayList<String>();
	 strings.add("Dies ist das mächtigste Schwert");
	 strings.add("          aller Zeiten.        ");
	 strings.add("Nutze es weise, junger Padawan!");
	 im.setLore(strings);
	 is.setItemMeta(im);
	 is.addUnsafeEnchantment(Enchantment.KNOCKBACK, 10);
	 is.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 10);
	 is.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 10);
	 return is;
 }
 
}
