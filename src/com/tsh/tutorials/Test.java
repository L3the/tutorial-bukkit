package com.tsh.tutorials;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.tsh.tutorials.commands.Command;

public class Test extends JavaPlugin implements Listener {

    private Objective objective;
    private Scoreboard scoreboard;

    @Override
    public void onEnable() {
        this.scoreboard = this.getServer().getScoreboardManager().getNewScoreboard();
        this.objective = this.scoreboard.registerNewObjective("test", "test");
        this.objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void join(PlayerJoinEvent event) {
        event.getPlayer().setScoreboard(this.scoreboard);
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
      
        final Score score = this.objective.getScore((Player) sender);
        score.setScore(score.getScore() + 1);
        return true;
    }
}