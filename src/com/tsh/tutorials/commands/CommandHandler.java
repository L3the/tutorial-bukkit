package com.tsh.tutorials.commands;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandHandler implements Listener{
	
	private ArrayList<Command> commandlist = new ArrayList<Command>();
	
	public CommandHandler()
	{
		registerCommand(new CommandTest());
		registerCommand(new CommandSpawn());
	}
	
	private void registerCommand(Command c)
	{
		commandlist.add(c);
	}
	
	@EventHandler
	void onCommand(PlayerCommandPreprocessEvent e)
	{
	 Player p = e.getPlayer();
	 String s = e.getMessage();
	 String[]args = s.split(" ");
	 
	 String command = args[0];
	 String[]argm = new String[args.length -1];
	 
	 for(int i = 1; i< args.length;i++)
	 {
		 argm[i-1] = args[i];
	 }
	 
	 if(command.startsWith("/"))
	 {
		 Command com = null;
		 
		 for(Command comm : this.commandlist)
		 {
			 if(comm.getCommand().equalsIgnoreCase(command))
			 {
				 com = comm;
			 }
		 }
		 
		 if(com != null)
		 {
			 if(com.getPermission() == null || e.getPlayer().hasPermission(com.getPermission()))
			 {
			 int arglength = argm.length;
			 
			 if(arglength < com.getMinArgs() )
			 {
				 e.getPlayer().sendMessage("�4Du hast zu wenig Argumente eingegeben. Bitte �berpr�fe deine Eingabe!");
			 }
			 
			 else if(arglength > com.getMaxArgs() )
			 {
				 e.getPlayer().sendMessage("�4Du hast zu viele Argumente eingegeben. Bitte �berpr�fe deine Eingabe!");
			 }
			 else
			 {
				 com.onCommandUse(p, argm);
			 }
			 }
			 else
				 e.getPlayer().sendMessage("Du hast keine Berechtigung daf�r.");
		 }
		 else
			 e.getPlayer().sendMessage("�4Dieser Befehl existiert nicht!");
	 }
		 
	}
	

}
