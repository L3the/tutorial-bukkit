package com.tsh.tutorials.commands;

import org.bukkit.entity.Player;

public abstract class Command 
{
  private int minarg;
  private int maxarg;
  
  public Command(int minargs,int maxargs)
  {
	  //�bergeben der Minimal und Maximalparameter
	  this.minarg = minargs;
	  this.maxarg = maxargs;
  }
  
  /**
   * 
   * @return Den minimal geforderten Wert.
   */
  public int getMinArgs()
  {
	  return minarg;
  }
  
  public int getMaxArgs()
  {
	  return maxarg;
  }
  
  /**
   * Gibt die Permission f�r den Command aus.
   * @return
   */
  public abstract String getPermission();
  
  /**
   * 
   * @param args - Die Parameter die dazu �bergeben werden.
   */
  public abstract void onCommandUse(Player p,String args[]);
  
  /**
   * Gibt den Commandstring zur�ck, der bei der Klasse eingetragen wurde.
   * @return
   */
  public abstract String getCommand();
  
  
  
}
