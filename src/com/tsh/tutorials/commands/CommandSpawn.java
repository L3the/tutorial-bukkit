package com.tsh.tutorials.commands;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.tsh.tutorials.TutorialBukkit;
import com.tsh.tutorials.YamlHandler;

public class CommandSpawn  extends Command
{

	public CommandSpawn() {
		super(0, 0);
	}

	@Override
	public String getPermission() {
		return null;
	}

	@Override
	public void onCommandUse(Player p, String[] args) 
	{
		FileConfiguration c = TutorialBukkit.getSpawnFile();
		Location l = p.getLocation();
		c.set("spawns.x", l.getBlockX());
		c.set("spawns.y", l.getBlockY());
		c.set("spawns.z", l.getBlockZ());
	    YamlHandler.saveYamlFile(c, TutorialBukkit.getRawSpawnFile());
	    p.sendMessage("Spawn erfolgreich gesetzt.");
	}

	@Override
	public String getCommand() {
		// TODO Auto-generated method stub
		return "/setspawn";
	}

}
